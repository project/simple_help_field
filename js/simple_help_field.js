/**
 * @file
 * Simple Help Field - Global functions.
 */

(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.SimpleHelpFieldCollapse = {
    attach: function (context, settings) {
      $('.header').click(function () {

        var $header = $(this);
        var $coll = $header.children('.js-simple-help-field-coll');
        var $content = $header.next();

        $header.text(function () {
          // Change class based on condition.
          $coll.toggleClass('coll-closed coll-opened');
        });

        // Open up the content needed - toggle the field.
        $content.slideToggle(500);
      });
    }
  };
})(jQuery, Drupal);
