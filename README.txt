CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Usage
 * Maintainers


INTRODUCTION
------------

The Simple Help Field module adds a new field to the standard Drupal form
fields to be used in help fields, description of requirements and
usage examples.

This new field should be used via Form API.


REQUIREMENTS
------------

No special requirements


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.


CONFIGURATION
-------------

 * Configure your field in Configuration » User interface » Simple Help Field

  - Set the field Theme.


USAGE
-------------

$form['simple_help_field_example'] = array(
  '#type' => 'simple_help',
  '#simple_help_title' => 'Title', // Optional.
  '#simple_help_theme' => 'simple-help-field-yellow', // Optional.
  '#value' => 'The text you want here',
  '#collapsible' => TRUE, // Optional, default TRUE.
  '#collapsed' => TRUE, // Optional, default FALSE.
);


MAINTAINERS
-----------

Current maintainers:
 * Carlos E Basqueira (cebasqueira) - https://www.drupal.org/user/3224891
